const { RC4 } = require("crypto-js");
const sha256 = require("crypto-js/sha256");
const { ec } = require("elliptic");
const { v1: uuidV1 } = require("uuid");

const ecInstance = new ec("secp256k1");

function genKeyPair() {
  return ecInstance.genKeyPair();
}

function generateId() {
  return uuidV1();
}

function hash(data) {
  return sha256(JSON.stringify(data)).toString();
}

function verifySignature({ publicKey, signature, dataHash }) {
  return ecInstance.keyFromPublic(publicKey, "hex").verify(dataHash, signature);  
}

module.exports = {
  genKeyPair,
  generateId,
  hash,
  verifySignature
};