const express = require("express");
const { json } = require("body-parser");
const { toString } = require("../blockchain/block");
const { blockchain } = require("../blockchain");
const { p2pServer } = require("./p2p");
const { wallet } = require("../wallet");
const { transactionPool } = require("../wallet/transaction-pool");
const { miner } = require("./miner");

const HTTP_PORT = process.env.HTTP_PORT || 3001;

const app = express();
const bc = blockchain();
const trxPool = transactionPool();
const userWallet = wallet({ bc, trxPool });
const p2p = p2pServer({ bc, trxPool });
const userMiner = miner({ bc, trxPool, userWallet, p2p });

app.use(json());

app.get("/blocks", function (req, res) {
  res.json(bc.getChain());
});

app.post("/mine", function (req, res) {
  const block = bc.addBlock(req.body.data);
  console.log("New block added:", toString(block));

  p2p.syncChains();
  res.json(bc.getChain());
});

app.get("/transactions", function (req, res) {
  res.json(trxPool.getTransactions());
});

app.post("/transact", function (req, res) {
  const { recipient, amount } = req.body;
  const transaction = userWallet.createTransaction({
    recipientAddr: recipient, amount
  });
  p2p.broadcastTransaction(transaction);
  res.json(trxPool.getTransactions());
});

app.get("/mine-transactions", function (req, res) {
  const block = userMiner.mine();
  console.log("New block added:", toString(block));
  res.json(bc.getChain());
})

app.get("/public-key", function (req, res) {
  res.json({ publicKey: userWallet.publicKey });
});

app.get("/balance", function (req, res) {
  const balance = userWallet.calculateBalance();
  res.json({ balance });
});

app.listen(HTTP_PORT, function () {
  console.log(`Listening on port ${HTTP_PORT}`);
});

p2p.listen();