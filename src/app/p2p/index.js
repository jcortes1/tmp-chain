const webSocket = require("ws");
const { blockchain } = require("../../blockchain");

const P2P_PORT = process.env.P2P_PORT || 5001;
const peers = process.env.PEERS ? process.env.PEERS.split(",") : [];
const MSG_TYPES = {
  chain: "CHAIN",
  transaction: "TRANSACTION",
  clear_trxs: "CLEAR_TRANSACTIONS"
};

function p2pServer({ bc, trxPool } = {}) {
  let sockets = [];

  function messageHandler(message) {
    const { type, chain, transaction } = JSON.parse(message);

    if (type === MSG_TYPES.chain) {
      bc.replaceChain(chain);

    } else if(type === MSG_TYPES.transaction) {
      trxPool.updateOrAddTransaction(transaction);

    } else if(type === MSG_TYPES.clear_trxs) {
      trxPool.clear();
    }
  }

  function socketHandler(socket) {
    console.log("socketHandler!!!");

    const chain = bc.getChain();
    const data = { type: MSG_TYPES.chain, chain };

    socket.on("message", messageHandler);
    socket.send(JSON.stringify(data));
    
    sockets = [...sockets, socket];
  }

  function openSocketHandler(socket) {
    return function () {
      return socketHandler(socket);
    };
  }
  
  function peerHandler(peer) {
    const socket = new webSocket(peer);
    socket.on("open", openSocketHandler(socket));
  }

  function listen() {
    const wsServer = new webSocket.Server({ port: P2P_PORT });
    wsServer.on("connection", socketHandler);
    
    // It iterates over when at least there is one peer in the array
    peers.forEach(peerHandler);
    console.log(`Listening for p2p connections on: ${P2P_PORT}`);
  }

  function syncChains() {
    const chain = bc.getChain();
    const data = { type: MSG_TYPES.chain, chain };

    sockets.forEach(function (socket) {
      socket.send(JSON.stringify(data));
    });
  }

  function broadcastTransaction(transaction) {
    const data = { type: MSG_TYPES.transaction, transaction };
    sockets.forEach(function (socket) {
      socket.send(JSON.stringify(data));
    });
  }

  function broadcastClearTransaction() {
    const data = { type: MSG_TYPES.clear_trxs };
    sockets.forEach(function (socket) {
      socket.send(JSON.stringify(data));
    });
  }

  const proto = {
    listen,
    syncChains,
    broadcastTransaction,
    broadcastClearTransaction
  };

  return Object.freeze(proto);
}

module.exports = {
  p2pServer
};