const { rewardTransaction } = require("../../wallet/transaction");
const { blockchainWallet } = require("../../wallet");


function miner({ bc, trxPool, userWallet, p2p }) {
  
  function mine() {
    const bcWallet = blockchainWallet();
    const rewardedTransaction = rewardTransaction({ minerWallet: userWallet, blockchainWallet: bcWallet });
    
    const validTransactions = [
      ...trxPool.validTransactions(),
      // include a reward for the miner
      rewardedTransaction
    ];

    // create a block consisting of the valid transactions
    const block = bc.addBlock(validTransactions);

    // synchronize the chains in the p2p server
    p2p.syncChains();

    // clear the transaction pool
    trxPool.clear();

    // broadcast to every miner to clear their transaction pools
    p2p.broadcastClearTransaction();

    return block;
  }

  const proto = { mine };

  return Object.freeze(proto);
}

module.exports = {
  miner
};