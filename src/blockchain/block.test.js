const { genesis, mineBlock, calculateDifficulty } = require("./block");

describe("Block", function () {
  let data, lastBlock, sndBlock;

  beforeEach(function () {
    data = "bar";
    lastBlock = genesis();
    sndBlock = mineBlock({ lastBlock, data });
  });

  it("sets the `data` to match the input", function () {
    const { data: blockData } = sndBlock;
    expect(blockData).toEqual(data);
  });

  it("sets the `lastHash` to match the hash of the last block", function () {
    const { lastHash: blockLastHash } = sndBlock;
    const { hash: lastBlockHash } = lastBlock;
    expect(blockLastHash).toEqual(lastBlockHash)
  });

  it("generates a hash that matches the difficulty", function () {
    const { hash, difficulty } = sndBlock;
    const zeros = "0".repeat(difficulty);
    expect(hash.substring(0, difficulty)).toEqual(zeros);
  });

  it("lowers the difficulty for slowly mined blocks", function () {
    const { timestamp, difficulty } = sndBlock;
    const currentTimestamp = timestamp + (60 * 60 * 1000);
    const calculatedDifficulty = calculateDifficulty({ lastBlock: sndBlock, currentTimestamp });
    expect(calculatedDifficulty).toEqual(difficulty - 1);
  });

  it("raises the difficulty for quickly mined blocks", function () {
    const { timestamp, difficulty } = sndBlock;
    const currentTimestamp = timestamp + 1;
    const calculatedDifficulty = calculateDifficulty({ lastBlock: sndBlock, currentTimestamp });
    expect(calculatedDifficulty).toEqual(difficulty + 1);
  });
});