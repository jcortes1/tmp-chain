const { hash } = require("../chain-util");
const { MINE_RATE, DIFFICULTY } = require("../config");

function toString({ timestamp, lastHash, hash, data, nonce, difficulty }) {
  return `Block -
    Timestamp : ${timestamp}
    Last Hash : ${lastHash.substring(0, 10)}
    Hash      : ${hash.substring(0, 10)}
    Nonce     : ${nonce}
    Difficulty: ${difficulty}
    Data      : ${JSON.stringify(data)}
  `;
}

function calculateDifficulty({ lastBlock, currentTimestamp }) {
  const { difficulty, timestamp } = lastBlock;
  if (timestamp + MINE_RATE > currentTimestamp) {
    return difficulty + 1;
  }
  return difficulty < 1 ? difficulty : difficulty - 1;
}

function generateHash({ timestamp, lastHash, data, nonce, difficulty }) {
  return hash(`${timestamp}${lastHash}${data}${nonce}${difficulty}`);
}

function genesis() {
  return block({
    timestamp: "Genesis time",
    lastHash: "------",
    hash: "f1r57-5g63",
    data: [],
    nonce: 0,
    difficulty: DIFFICULTY
  });
}

function mineBlock({ lastBlock, data }) {
  const { hash: lastHash } = lastBlock;
  let timestamp;
  let hash;
  let nonce = 0;
  let difficulty;
  let zeros;

  do {
    nonce += 1;
    timestamp = Date.now();
    difficulty = calculateDifficulty({ lastBlock, currentTimestamp: timestamp });
    zeros = "0".repeat(difficulty);
    hash = generateHash({ timestamp, lastHash, data, nonce, difficulty });

  } while (hash.substring(0, difficulty) !== zeros);
  
  return block({ timestamp, lastHash, hash, data, nonce, difficulty });
}

function block({ timestamp, lastHash, hash, data, nonce, difficulty = DIFFICULTY }) {
  return Object.freeze({ timestamp, lastHash, hash, data, nonce, difficulty });
}

module.exports = {
  block,
  genesis,
  mineBlock,
  generateHash,
  calculateDifficulty,
  toString,
};