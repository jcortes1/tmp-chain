const { blockchain, isValidChain } = require(".");
const { genesis, block } = require("./block");

describe("Blockchain", function () {
  let bc, bc2;

  beforeEach(function () {
    bc = blockchain();
    bc2 = blockchain();
  });

  it("starts with genesis block", function () {
    const [fstBlock] = bc.getChain();
    expect(fstBlock).toEqual(genesis());  
  });

  it("adds a new block", function () {
    const data = "foo";
    const lastBlock = bc.addBlock(data);
    const { data: lastBlockData } = lastBlock;
    expect(lastBlockData).toEqual(data);
  });

  it("validates a valid chain", function () {
    bc2.addBlock("foo");
    const validChain = bc2.getChain();
    expect(isValidChain(validChain)).toBe(true);
  });

  it("invalidates a chain with a corrupt genesis block", function () {
    const [, ...restOfBlocks] = bc2.getChain();
    const badGenesisBlock = block({
      timestamp: "Genesis time",
      lastHash: "------",
      hash: "f1r57-5g63",
      data: "Bad data"
    });
    const invalidChain = [badGenesisBlock, ...restOfBlocks];
    expect(isValidChain(invalidChain)).toBe(false);
  });

  it("invalidates a chain with a corrupt block", function () {
    bc2.addBlock("foo");
    const validChain = bc2.getChain();
    const [genesisBlock, sndBlock] = validChain;

    const { timestamp, lastHash, hash } = sndBlock;
    const badBlock = block({ timestamp, lastHash, hash, data: "Not foo" });

    const invalidChain = [genesisBlock, badBlock];
    expect(isValidChain(invalidChain)).toBe(false);
  });

  it("replaces the chain a valid chain", function () {
    bc2.addBlock("goo");
    const validChain = bc2.getChain();
    const replacedChain = bc.replaceChain(validChain);
    expect(validChain).toEqual(replacedChain);
  });

  it("does not replace a chain with one or less than or equal to length", function () {
    bc.addBlock("goo");
    const replacedChain = bc.replaceChain(bc2.getChain());

    expect(replacedChain).not.toEqual(bc2.getChain());
  });
});