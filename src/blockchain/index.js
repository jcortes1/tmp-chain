const { generateHash, genesis, mineBlock } = require('./block');

function isValidChain(chain) {
  const [genesisBlock] = chain;

  if (JSON.stringify(genesisBlock) !== JSON.stringify(genesis())) {
    return false;
  }

  const validations =
    chain
      .reduce(function (validations, prevBlock, idx, blocks) {
        const nextBlock = blocks[idx + 1];

        if (validations.includes(false) || !nextBlock) {
          return validations;
        }

        const { lastHash: nextBlockLastHash, timestamp, data, hash, nonce, difficulty } = nextBlock;
        const { hash: prevBlockHash } = prevBlock;
        const generatedHash = generateHash({ lastHash: nextBlockLastHash, timestamp, data, nonce, difficulty });
        
        if (prevBlockHash !== nextBlockLastHash) {
          console.log("prevBlockHash !== nextBlockLastHash");
          return [...validations, false];
        }
        
        if (generatedHash !== hash) {
          return [...validations, false];
        }

        return [...validations, true];
      }, []);
  
  const isValid =
    validations.every(function (valid) {
      return valid;
    });

  return isValid;
}

function blockchain() {
  let chain = [genesis()];

  function addBlock(data) {
    const [lastBlock] = chain.slice().reverse();
    const currentBlock = mineBlock({ lastBlock, data });
    chain = [...chain, currentBlock];
    return currentBlock;
  }

  function getChain() {
    return chain.slice();
  }

  function replaceChain(newChain = []) {
    if (newChain.length <= chain.length) {
      console.log("The new chain is not longer than the current chain.");
      return chain.slice();
    }

    if (!isValidChain(newChain)){
      console.log("The new chain is not valid.");
      return newChain.slice();
    }
    
    console.log("Replacing blockchain with the new chain.");
    chain = newChain;
    return chain.slice();
  }

  const proto = {
    addBlock,
    getChain,
    replaceChain,
  };

  return Object.freeze(proto);  
}

module.exports = {
  blockchain,
  isValidChain
};