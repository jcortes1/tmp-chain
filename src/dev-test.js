const { blockchain } = require("./blockchain");
const { toString } = require("./blockchain/block");

const bc = blockchain();

[...Array(10).keys()]
  .forEach(idx => {
    console.log(toString(bc.addBlock(`foo ${idx}`)));
  });