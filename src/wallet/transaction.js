const { MINING_REWARD, TRX_FEE } = require("../config");
const { generateId, hash, verifySignature } = require("../chain-util");

function signTransaction({ transactionOutputs, senderWallet }) {
  const dataHash = hash(transactionOutputs);
  return Object.freeze({
    timestamp: Date.now(),
    amount: senderWallet.getBalance(),
    address: senderWallet.publicKey,
    signature: senderWallet.sign(dataHash)
  });
}

function verifyTransaction({ transactionInput, transactionOutputs = [] }) {
  const { address, signature } = transactionInput;
  const dataHash = hash(transactionOutputs);
  return verifySignature({ publicKey: address, dataHash, signature });
}

function createTransaction({ senderWallet, recipientAddr, amount }) {
  const { publicKey: senderWalletPublicKey } = senderWallet;
  const senderWalletBalance = senderWallet.getBalance();

  if (amount > senderWalletBalance) {
    console.log(`Amount: ${amount} exceeds balance.`);
    return;
  }

  const id = generateId();
  const outputs = [
    { amount: senderWalletBalance - amount, address: senderWalletPublicKey },
    { amount, address: recipientAddr }
  ];
  const input = signTransaction({ transactionOutputs: outputs, senderWallet });
  
  return transaction({ id, input, outputs });
}

function updateTransaction({ id, senderWallet, transactionOutputs, recipientAddr, amount }) {
  const senderOutput = transactionOutputs.find(function (output) {
    return output.address === senderWallet.publicKey;
  });

  const receiptOutputs = transactionOutputs.filter(function (output) {
    return output.address !== senderWallet.publicKey;
  });
  
  const { amount: senderOutputAmount, address: senderOutputAddress } = senderOutput;

  if (amount > senderOutputAmount) {
    console.log(`Amount: ${amount} exceeds balance.`);
    return;
  }
  
  const outputs = [
    { amount: senderOutputAmount - amount, address: senderOutputAddress },
    ...receiptOutputs,
    { amount, address: recipientAddr },
  ];

  const input = signTransaction({ senderWallet, transactionOutputs: outputs });

  return transaction({ id, input, outputs });
}

function rewardTransaction({ minerWallet, blockchainWallet }) {
  const id = generateId();
  const outputs = [{
    amount: MINING_REWARD, address: minerWallet.publicKey
  }];
  const input = signTransaction({ transactionOutputs: outputs, senderWallet: blockchainWallet });

  return transaction({ id, input, outputs });
}

function transaction({ id, input, outputs }) {
  return Object.freeze({ id, input, outputs });
}

module.exports = {
  transaction,
  createTransaction,
  verifyTransaction,
  updateTransaction,
  rewardTransaction
};