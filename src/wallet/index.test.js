const { wallet } = require(".");
const { blockchain } = require("../blockchain");
const { transactionPool } = require("./transaction-pool");
const { INITIAL_BALANCE } = require("../config");

describe("Wallet", function () {
  let senderWallet, trxPool, bc;

  beforeEach(function () {
    trxPool = transactionPool();
    bc = blockchain();
    senderWallet = wallet({ bc, trxPool });
    // senderWallet: 500
  });

  describe("creating a transaction", function () {
    let transaction, sendAmount, recipientAddr;

    beforeEach(function () {
      sendAmount = 50;
      recipientAddr = "r4nd0m-4ddr355";
      transaction = senderWallet.createTransaction({
        amount: sendAmount, recipientAddr
      });
    });

    describe("and doing the same transaction", function () {
      beforeEach(function () {
        transaction = senderWallet.createTransaction({
          amount: sendAmount, recipientAddr
        });
      });

      it("doubles the `sendAmount` substracted from the wallet balance", function () {
        const senderOutput = transaction.outputs.find(function (output) {
          return output.address === senderWallet.publicKey;
        });
        expect(senderOutput.amount).toEqual(senderWallet.getBalance() - (sendAmount * 2));
      });

      it("clones the `sendAmount` output for the recipient", function () {
        const recipientAmounts = transaction.outputs
          .filter(function (output) {
            return output.address === recipientAddr;
          })
          .map(function ({ amount }) {
            return amount;
          });
        expect(recipientAmounts).toEqual([sendAmount, sendAmount]);
      });
    });
  });

  describe("calculating a balance", function () {
    let anotherSenderWallet, addBalance, repeatAdd;

    beforeEach(function () {
      addBalance = 100;
      repeatAdd = 3;
      anotherSenderWallet = wallet({ bc, trxPool });
      // senderWallet: 500
      // anotherSenderWallet: 500

      [...Array(repeatAdd).keys()]
        .forEach(function () {
          anotherSenderWallet.createTransaction({ recipientAddr: senderWallet.publicKey, amount: addBalance });
        });
        
      bc.addBlock(trxPool.getTransactions());
      // senderWallet: 500 + 300 = 800
      // anotherSenderWallet: 500 - 300 = 200
    });

    it("calculates the balance for blockchain transactions mtching the recipient", function () {
      const calculatedBalance = senderWallet.calculateBalance();
      expect(calculatedBalance).toEqual(INITIAL_BALANCE + (addBalance * repeatAdd));
    });

    it("calculates the balance for blockchain transactions matching the sender", function () {
      const calculatedBalance = anotherSenderWallet.calculateBalance();
      expect(calculatedBalance).toEqual(INITIAL_BALANCE - (addBalance * repeatAdd));
    });

    describe("and the recipient conducts a transaction", function () {
      let substractBalance, recipientBalance;

      beforeEach(function () {
        trxPool.clear();
        substractBalance = 60;
        recipientBalance = senderWallet.calculateBalance();

        senderWallet.createTransaction({ recipientAddr: anotherSenderWallet.publicKey, amount: substractBalance });
        bc.addBlock(trxPool.getTransactions());
        // senderWallet: 800 - 60 = 740
        // anotherSenderWallet: 200 + 60 = 260
      });

      it("calculates the balance for blockchain transactions mtching the recipient", function () {
        const calculatedBalance = senderWallet.calculateBalance();
        expect(calculatedBalance).toEqual(recipientBalance - substractBalance);
      });
      
      it("calculates the balance for blockchain transactions matching the sender", function () {
        const calculatedBalance = anotherSenderWallet.calculateBalance();
        expect(calculatedBalance).toEqual(INITIAL_BALANCE - (addBalance * repeatAdd) + substractBalance);
      });

      describe("and the sender sends another transaction to the recipient", function () {

        beforeEach(function () {
          trxPool.clear();
          anotherSenderWallet.createTransaction({ recipientAddr: senderWallet.publicKey, amount: addBalance });
          bc.addBlock(trxPool.getTransactions());
          // senderWallet: 740 + 100 = 840
          // anotherSenderWallet: 260 - 100 = 160
        });

        it("calculates the recipient balance only using transaction since its most recent one", function () {
          const calculatedBalance = senderWallet.calculateBalance();
          expect(calculatedBalance).toEqual(recipientBalance - substractBalance + addBalance);
        });

        it("calculates the balance for blockchain transactions matching the sender", function () {
          const calculatedBalance = anotherSenderWallet.calculateBalance();
          expect(calculatedBalance).toEqual(INITIAL_BALANCE - (addBalance * repeatAdd) + substractBalance - addBalance);
        });
      });
    });
  });
});