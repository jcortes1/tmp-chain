const { blockchain } = require("../blockchain");
const { transactionPool } = require("./transaction-pool");
const { updateTransaction } = require("./transaction");
const { wallet } = require(".");

describe("Transaction Pool", function () {
  let bc, trxPool, senderWallet, transaction;

  beforeEach(function () {
    const amount = 20;
    const recipientAddr = "r4nd-4dr355";

    bc = blockchain();
    trxPool = transactionPool();
    senderWallet = wallet({ bc, trxPool });
    transaction = senderWallet.createTransaction({ recipientAddr, amount })
  });

  it("adds a transaction to the pool", function () {
    const transactions = trxPool.getTransactions();
    const foundTrx = transactions.find(function ({ id }) {
      return id === transaction.id;
    });
    expect(foundTrx).toEqual(transaction);
  });

  it("updates a transaction in the pool", function () {
    const oldTrx = JSON.stringify(transaction);
    const { id, outputs: transactionOutputs } = transaction;
    const newTrx = updateTransaction({
      id, senderWallet,
      amount: 40,
      recipientAddr: "foo-4ddr355",
      transactionOutputs
    });

    trxPool.updateOrAddTransaction(newTrx);
    const transactions = trxPool.getTransactions();
    const foundTrx = transactions.find(function ({ id }) {
      return id === newTrx.id;
    });
    expect(JSON.stringify(foundTrx)).not.toEqual(oldTrx);
  });

  it("clears transactions", function () {
    trxPool.clear();
    expect(trxPool.getTransactions()).toEqual([]);
  });

  describe("mixing valid and corrupt transactions", function () {
    let validTransactions;

    beforeEach(function () {
      validTransactions = [...trxPool.getTransactions()];

      [...Array(6).keys()]
        .forEach(function (idx) {
          senderWallet = wallet({ bc, trxPool });
          transaction = senderWallet.createTransaction({
            recipientAddr: "r4nd-4dr355", amount: 30
          });
          if (idx % 2 === 0) {
            // transaction.input.amount = 99999;
            const { id, input, outputs } = transaction;
            const corruptedInput = { ...input, amount: 99999 };
            trxPool.corruptTransaction({ id, input: corruptedInput, outputs });

          } else {
            validTransactions = [...validTransactions, transaction];
          }
        });
    });

    it("shows a difference between valid and corrupt transactions", function () {
      expect(JSON.stringify(trxPool.getTransactions())).not.toEqual(JSON.stringify(validTransactions));
    });

    it("grabs valid transactions", function () {
      expect(JSON.stringify(trxPool.validTransactions())).toEqual(JSON.stringify(validTransactions));
    })
  });
});