const { INITIAL_BALANCE } = require("../config");
const { genKeyPair } = require("../chain-util");
const { createTransaction: createTrx, updateTransaction: updateTrx } = require("./transaction");

function toString({ getBalance, publicKey }) {
  return `Wallet -
    Public Key : ${publicKey}
    Balance    : ${getBalance()}
  `;
}

function blockchainWallet() {
  const address = "tmp-blockchain-wallet";
  const bcWallet = wallet();
  return Object.freeze({ ...bcWallet, address });
}

function wallet({ bc = {}, trxPool = {} } = {}) {
  let balance = INITIAL_BALANCE;
  const keyPair = genKeyPair();
  const publicKey = keyPair.getPublic().encode("hex");

  function getBalance() {
    return balance;
  }

  function sign(dataHash) {
    return keyPair.sign(dataHash);
  }

  function createTransaction({ recipientAddr, amount }) {
    balance = calculateBalance();

    if (amount > balance) {
      console.log(`Amount: ${amount} exceeds the current balance: ${balance}.`);
      return;
    }
  
    let transaction = trxPool.getTransaction(publicKey);
  
    if (transaction) {
      const { id, outputs: transactionOutputs } = transaction;
      transaction = updateTrx({
        id, senderWallet: proto,
        transactionOutputs, recipientAddr, amount
      });
    } else {
      transaction = createTrx({ senderWallet: proto, recipientAddr, amount });
    }

    trxPool.updateOrAddTransaction(transaction);

    return transaction;
  }

  function calculateBalance() {
    let calculatedBalance = getBalance();

    const transactions =
      bc.getChain()
        .reduce(function (trxs, block) {
          return [...trxs, ...block.data];
        }, []);

    const walletInputTrxs = transactions.filter(function (trx) {
      return trx.input.address === publicKey;
    });

    let startTime = 0;

    if (walletInputTrxs.length > 0) {
      const recentInpuTrx = walletInputTrxs.reduce(function (prevTrx, currentTrx) {
        return prevTrx.input.timestamp > currentTrx.input.timestamp
          ? prevTrx : currentTrx;
      });

      const outputsTotal = recentInpuTrx.outputs.reduce(function (total, output) {
        return output.address === publicKey
          ? total + output.amount
          : total;
      }, 0);

      calculatedBalance = outputsTotal;
      startTime = recentInpuTrx.input.timestamp;
    }

    const trxsTotalAmount = transactions.reduce(function (total, trx) {
      if (trx.input.timestamp > startTime) {
        const outputsTotal = trx.outputs.reduce(function (total, output) {
          return output.address === publicKey
            ? total + output.amount
            : total;
        }, 0);
        return total + outputsTotal;
      }
      return total;
    }, 0);

    return calculatedBalance + trxsTotalAmount;
  }

  const proto = {
    publicKey,
    sign,
    createTransaction,
    calculateBalance,
    getBalance
  };

  return Object.freeze(proto);
}

module.exports = {
  toString,
  wallet,
  blockchainWallet
};