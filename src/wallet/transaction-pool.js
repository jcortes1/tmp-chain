const { verifyTransaction } = require("./transaction");

function transactionPool() {
  let transactions = [];

  function updateOrAddTransaction(transaction) {
    const { id: transactionId } = transaction || {};

    const { found: transactionFound, otherTrxs: otherTransactions } =
      transactions.reduce(function (obj, trx) {
        if (trx.id === transactionId) {
          return { found: trx, otherTrxs: obj.otherTrxs };
        }
        return { found: obj.found, otherTrxs: [...obj.otherTrxs, trx] };
      }, { found: undefined, otherTrxs: [] });

    if (transactionFound) {
      transactions = [ ...otherTransactions, transaction ];
    } else {
      transactions = [ ...transactions, transaction ];
    }
    return transactions;
  }

  function getTransactions() {
    return transactions;
  }

  function getTransaction(address) {
    const transaction = transactions.find(function ({ input }) {
      return input.address === address;
    });
    return transaction;
  }

  function validTransactions() {
    return transactions.filter(function (trx) {
      const outputTotal = trx.outputs.reduce(function (total, output) {
        return total + output.amount;
      }, 0);

      if (trx.input.amount !== outputTotal) {
        console.log(`Invalid transaction from ${trx.input.address}`);
        return;
      }

      if (!verifyTransaction({ transactionInput: trx.input, transactionOutputs: trx.outputs })) {
        console.log(`Invalid signature from ${input.address}`);
        return;
      }

      return trx;
    });
  }

  // Warning: Dangerous function!
  function corruptTransaction(corruptedTrx) {
    transactions = transactions.map(function (trx) {
      if (trx.input.address === corruptedTrx.input.address) {
        return corruptedTrx;
      }
      return trx;
    });
  }

  function clear() {
    transactions = [];
  }

  const proto = {
    updateOrAddTransaction,
    getTransactions,
    getTransaction,
    validTransactions,
    corruptTransaction,
    clear
  };

  return Object.freeze(proto);
}

module.exports = {
  transactionPool
};