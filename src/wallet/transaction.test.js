const { wallet, blockchainWallet } = require(".");
const { blockchain } = require("../blockchain");
const { createTransaction, verifyTransaction, updateTransaction, rewardTransaction } = require("./transaction");
const { MINING_REWARD } = require("../config");

describe("Transaction", function () {
  let bc, transaction, senderWallet, recipientAddr, amount;

  beforeEach(function () {
    bc = blockchain();
    senderWallet = wallet({ bc });
    amount = 50;
    recipientAddr = "r3c1p13nt";
    transaction = createTransaction({ senderWallet, amount, recipientAddr });
  });

  it("outputs the `amount` substracted from the wallet balance", function () {
    const outputTrx = transaction.outputs.find(function (output) {
      return output.address === senderWallet.publicKey;
    });
    expect(outputTrx.amount).toEqual(senderWallet.getBalance() - amount);
  });

  it("outputs the `amount` added to the recipient", function () {
    const outputTrx = transaction.outputs.find(function (output) {
      return output.address === recipientAddr;
    });
    expect(outputTrx.amount).toEqual(amount);
  });

  it("inputs the balance of the wallet", function () {
    expect(transaction.input.amount).toEqual(senderWallet.getBalance());
  });

  it("validates a valid transaction", function () {
    const { input: transactionInput, outputs: transactionOutputs } = transaction;
    const isValid = verifyTransaction({ transactionInput, transactionOutputs });
    expect(isValid).toBe(true);
  });

  it("invalidates a corrupt transaction", function () {
    const { input: transactionInput, outputs } = transaction;
    const [fstOutput, sndOutput] = outputs;
    const fakeFstOutput = { ...fstOutput, amount: 5000 };
    const fakeOutputs = [fakeFstOutput, sndOutput];
    const isValid = verifyTransaction({ transactionInput, transactionOutputs: fakeOutputs });
    expect(isValid).toBe(false);
  });
});

describe("Transaction with an amount that exceeds the balance", function () {
  let bc, transaction, senderWallet, recipientAddr, amount;

  beforeEach(function () {
    bc = blockchain();
    senderWallet = wallet({ bc });
    amount = 50000;
    recipientAddr = "r3c1p13nt";
    transaction = createTransaction({ senderWallet, amount, recipientAddr });
  });
  
  it("does not create the transaction", function () {
    expect(transaction).toEqual(undefined);
  });
});

describe("Updating a transaction", function () {
  let bc, transaction, senderWallet, recipientAddr, amount, nextRecipientAddr, nextAmount;

  beforeEach(function () {
    bc = blockchain();
    senderWallet = wallet({ bc });
    amount = 50;
    recipientAddr = "r3c1p13nt";
    transaction = createTransaction({ senderWallet, amount, recipientAddr });

    const { id, outputs: transactionOutputs } = transaction;
    nextAmount = 20;
    nextRecipientAddr = "n3xt-4ddr355";
    transaction = updateTransaction({
      id, senderWallet, transactionOutputs,
      recipientAddr: nextRecipientAddr, amount: nextAmount
    });
  });
  
  it("substracts the next amount from the sender's output", function () {
    const senderOutput = transaction.outputs.find(function (output) {
      return output.address === senderWallet.publicKey;
    });
    
    expect(senderOutput.amount).toEqual(senderWallet.getBalance() - amount - nextAmount);
  });

  it("outputs an amount for the next recipient", function () {
    const recipientOutput = transaction.outputs.find(function (output) {
      return output.address === nextRecipientAddr;
    });
    expect(recipientOutput.amount).toEqual(nextAmount);
  });

  describe("creating a reward transaction", function () {
    beforeEach(function () {
      const bcWallet = blockchainWallet();
      transaction = rewardTransaction({ minerWallet: senderWallet, blockchainWallet: bcWallet });
    });

    it("rewards the miner's wallet", function () {
      const rewardedTransaction = transaction.outputs.find(function (output) {
        return output.address === senderWallet.publicKey;
      });
      expect(rewardedTransaction.amount).toEqual(MINING_REWARD);
    });
  });
});
