# Mindless Chain

## WebSockets
```
$ yarn install
$ yarn dev
$ HTTP_PORT=3002 P2P_PORT=5002 PEERS=ws://localhost:5001 yarn dev
$ HTTP_PORT=3003 P2P_PORT=5003 PEERS=ws://localhost:5001,ws://localhost:5002 yarn dev
```

## Get Blocks
```
$ curl -X GET http://localhost:3001/blocks | json_pp
```

## Post Mine a Block
```
$ curl -X POST -H "Content-Type: application/json" \
  -d '{ "data": "foo" }' \
  http://localhost:3001/mine | json_pp
```

## Get Transactions form the Pool
```
$ curl -X GET http://localhost:3001/transactions | json_pp
```

## Post a Transactions in the Pool
```
$ curl -X POST -H "Content-Type: application/json" \
  -d '{ "recipient": "foo-4dr355", "amount": 30 }' \
  http://localhost:3001/transact | json_pp

$ curl -X POST -H "Content-Type: application/json" \
  -d '{ "recipient": "bar-4dr355", "amount": 50 }' \
  http://localhost:3002/transact | json_pp
```

# Get Public Key
```
$ curl -X GET http://localhost:3001/public-key | json_pp
```

# GET Mine Transactions
```
$ curl -X GET http://localhost:3002/mine-transactions | json_pp
```

# GET Balance
```
$ curl -X GET http://localhost:3001/balance | json_pp
```
